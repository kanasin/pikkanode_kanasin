const session = require('koa-session');
const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const render = require('koa-ejs')
const mysql = require('mysql2/promise')
const path = require('path')
const koaBody = require('koa-body')
const cors = require('@koa/cors')


const signIn = require('./controllers/signin')
const signUp = require('./controllers/signup')
const create = require('./controllers/create')
const home = require('./controllers/index')
const api = require('./controllers/apimanager')


const app = new Koa()
const router = new Router()
const pictureDir = path.join(__dirname, 'public', 'img')
const pool = mysql.createPool({
    connectionLimit : 10,
    host            : 'localhost',
    user            : 'root',
    password        : '',
    database        : 'pikkanode'
})

signInManage = signIn(pool)
signUpManage = signUp(pool)
homeManage = home(pool)
createManage = create(pool, pictureDir)
apiManage = api(pool)
app.keys = ['user']


//web manage
router.get('/', homeManage.showHome)
router.get('/signin', isAnonymous, signInManage.signInPage)
router.post('/signin', signInManage.signInPost)
router.get('/signup', isAnonymous, signUpManage.signUpPage)
router.post('/signup', signUpManage.register)
router.get('/create', isUser, createManage.uploadPage)
router.post('/create', createManage.uploadFile)
router.get('/pikka/:id', isUser, homeManage.directPic)
router.post('/pikka', isUser, homeManage.likeAndComment)
router.post('/facebook_login', isAnonymous, signInManage.facebookLogIn)
router.get('/profile', isUser, homeManage.showProfile)
router.get('/signout', ctx => {
    delete ctx.session.id
    ctx.redirect('/')
})


//API manage
router.post('/api/v1/auth/signup', apiManage.signup)
router.post('/api/v1/auth/signin', apiManage.signin)
router.post('/api/v1/auth/signout', apiManage.signOut)
router.get('/api/v1/pikka', apiManage.getPikka)
router.post('/api/v1/pikka', apiManage.postPikka)
router.get('/api/v1/pikka/:id', apiManage.getPikById)
router.post('/api/v1/pikka/:id/comment', apiManage.addComment)
router.put('/api/v1/pikka/:id/like', apiManage.addLike)
router.delete('/api/v1/pikka/:id/like', apiManage.unLike)



async function flash(ctx, next){
    ctx.flash = ctx.session.flash
    delete ctx.session.flash
    await next()
}
async function isUser(ctx, next){
    if(!ctx.session.id){
        ctx.redirect('/')
    }
    await next()
}
async function isAnonymous(ctx, next){
    if(ctx.session.id){
        ctx.redirect('/')
    }
    await next()
}

app.use(cors({
     //origin: 'https://storage.googleapis.com',
    allowMethods: ['GET', 'POST', 'PUT', 'DELETE'],
   // allowHeaders: ['Authorization', 'Content-Type'],
    // maxAge: 3600,
    credentials: true
}))


app.use(session(app))
app.use(koaBody({multipart: true}))
app.use(router.routes())
app.use(router.allowedMethods())


app.use(flash)
app.use(serve(path.join(__dirname, 'public')))

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})








app.listen(3000)