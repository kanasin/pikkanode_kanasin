module.exports = {
    async showAll(pool){
        let sql = 'select p.id, p.caption, p.picture_name, p.created_at, u.email, (select count(*) from likes where picture_id = p.id) as likes, (select count(*) from comments where picture_id = p.id) as comment from pictures p inner join users u on p.created_by = u.id'
        let [rows] = await pool.query(sql)
        return rows
    },
    async findById(pool, id){
        let sql = 'select p.id, p.caption, p.picture_name, p.created_at, u.email, (select count(*) from likes where picture_id = p.id) as likes, (select count(*) from comments where picture_id = p.id) as comment from pictures p inner join users u on p.created_by = u.id where p.id = ?'
        let [rows] = await pool.query(sql, [id])
        return rows
    },
    async findComments(pool, id){
        [rows] = await pool.query('select text, email from comments inner join users on comments.created_by = users.id where picture_id = ?', [id])
        return rows
        
    },
    async searchCaption(pool, caption){
        let sql = 'select p.id, p.caption, p.picture_name, p.created_at, u.email, (select count(*) from likes where picture_id = p.id) as likes, (select count(*) from comments where picture_id = p.id) as comment from pictures p inner join users u on p.created_by = u.id where p.caption like ?'
        let [rows] = await pool.query(sql, [`%${caption}%`])
        return rows
    },
    async addComment(pool, dataArray){
        let sql = 'select * '
    },
    async checkLike(pool, id, picId){
        let [rows] = await pool.query(
            `select count(*) as num from likes where user_id = ? and picture_id = ?`,
            [id, picId]
        )
        if(rows[0].num>0){
            await pool.query(
                `delete from likes where user_id = ? and picture_id = ?`,
                [id, picId]
            )
        }else if(rows[0].num==0){
            await pool.query(
                `insert into likes(user_id, picture_id) value(?,?)`,
                [id, picId]
            )
        }
        let [row] = await pool.query(
            `select count(*) as num from likes where picture_id = ?`,
            [picId]
        )
        return row[0].num
    },
    async addComment(pool, createBy, picId, text){
        await pool.query(
            `insert into comments(created_by, picture_id, text) value(?,?,?)`,
            [createBy, picId, text]
        )
    },
    async getProfileById(pool, id){
        const [row] = await pool.query(
            `select * from users where id = ?`,
            [id]
        )
        return row[0]
    }

    
}