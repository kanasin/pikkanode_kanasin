const uuid4 = require('uuid/v4')
const fs = require('fs-extra')
const path = require('path')

class imageManage {
    constructor(pool, row) {
        this._pool = pool;
         
        this.userId = row.user
        this.file = row.pathFile,
        this.caption = row.caption
        this.pictureDir = row.pictureDir
    }
    async uploadFile() {
        let filename = uuid4()
        await this._pool.query(
            'insert into pictures(picture_name, caption, created_by) value(?,?,?)', 
            [filename, this.caption, this.userId]
        )
        await fs.copy(this.file, path.join(this.pictureDir, filename))
    }
    
}




module.exports = {
    async insertPhoto(pool, data){
        const upload = new imageManage(pool, data)
        await upload.uploadFile()
    }
}