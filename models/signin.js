const bcrypt = require('bcrypt')

module.exports = {
    async checkLogin(pool, userName, passWord){
        let [rows] = await pool.query(
            `select * from users where email = ?`,
            [userName]
        )
        if(rows.length > 0){
            let check = await bcrypt.compare(passWord, rows[0].password)
            if(check){
                return rows
            }else{
                return false
            }
        }else{
            return false
        }
        
    },
    async checkEmail(pool, email){
        const [row] = await pool.query(
            `select count(*) as num_row from users where email = ?`,
            [email]
        )
        if(row[0].num_row > 0){
            return false
        }else{
            return true
        }
    },
    async fbRegister(pool, arrayData){
        await pool.query(
            `insert into users(email, first_name, last_name, facebook_user_id) value(?, ?, ?, ?)`,
            arrayData
        )
    },
    async fbLogin(pool, email){
        const [row] = await pool.query(
            `select * from users where email = ?`,
            [email]
        )
        return row[0]
    }

    
}