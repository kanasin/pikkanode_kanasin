module.exports = {
    async checkEmail(pool, email){
        let [rows] = await pool.query(
            `select count(*) as row from users where email = ?`,
            [email]
        )
        return rows
    },
    async insertUser(pool, arrayData){
        let success = await pool.query(`insert into users(email, password)  value(?,?)`, arrayData)
        if(success){
            return true
        }else{
            return false
        }
    },
    async getIdByEmail(pool, email){
        let [row] = await pool.query(
            `select id from users where email = ?`,
            [email]
        )
        return row[0].id
    },
    async getAllByEmail(pool, email){
        let [row] = await pool.query(
            `select * from users where email = ?`,
            [email]
        )
        return row
    },
    async getAll(pool){
        const [rows] = await pool.query(
            `select p.id, p.caption, p.picture_name, p.created_at, u.email, (select count(*) from likes where picture_id = p.id) as likes, (select count(*) from comments where picture_id = p.id) as comment 
            from pictures p inner join users u on p.created_by = u.id`
        )
        let newData = rows.map( val => {
            let id = val.picture_name
            val.picture_name = `http://localhost:3000/img/${val.picture_name}`
            return {id: id, caption: val.caption, picture: val.picture_name, createdAt: val.created_at, commentCount: val.comment, likeCount: val.likes}
        })
        return newData
    },
    async apiCreate(pool, arrayData){
        await pool.query(
            'insert into pictures(picture_name, caption, created_by) value(?,?,?)', 
            arrayData
        )
        const [row] = await pool.query(
            `select id, created_at from pictures where id = (select max(id) from pictures)`
        )
        let newObj = {id: row[0].id, createdAt: row[0].created_at}
        return newObj
    },
    async getPikById(pool, id){
        let [row] = await pool.query(
            `select p.id, p.caption, p.picture_name, p.created_at, u.email, 
            (select count(*) from likes where picture_id = p.id) as likes, 
            (select count(*) from comments where picture_id = p.id) as comment 
            from pictures p inner join users u on p.created_by = u.id 
            where p.id = ?`,
            [id]
        )
        return row[0]
    },
    async getCommentById(pool, id){
        let [rows] = await pool.query(
            `select * from comments where picture_id = ?`,
            [id]
        )
        return rows
    },
    async countPicById(pool, id){
        let [row] = await pool.query(
            `select count(id) as num from pictures where id = ?`,
            [id]
        )
        return row[0].num
    },
    async lastedComment(pool){
        const [row] = await pool.query(
            `select * from comments where id = (select max(id) from comments)`
        )
        return {commentId: row[0].id, createdAt: row[0].created_at}
    },
    async addComment(pool, arrayData){
        await pool.query(
            `insert into comments(text, picture_id, created_by) value(?,?,?)`,
            arrayData
        )
    },
    async addLike(pool, arrayData){
        await pool.query(
            `insert into likes(picture_id, user_id) value(?, ?)`,
            arrayData
        )
    },
    async countLikeByPicId(pool, arrayData){
        let [row] = await pool.query(
            `select count(*) as num from likes where picture_id = ? and user_id = ?`,
            arrayData
        )
        return row[0].num
    },
    async unLike(pool, arrayData){
        await pool.query(
            `delete from likes where picture_id = ? and user_id = ?`,
            arrayData
        )
    }
}