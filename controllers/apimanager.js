const bcrypt = require('bcrypt')
const apiModel = require('./../models/apimanager')
const uuid4 = require('uuid/v4')
const fs = require('fs-extra')
const path = require('path')
const pictureDir = path.join(__dirname, 'public', 'img')

module.exports = function (pool) {
    return {
        async signup(ctx, next) {
            if (ctx.request.body.email && ctx.request.body.password) {
                let reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/
                if (reg.test(ctx.request.body.email) == false) {
                    ctx.status = 400
                    ctx.body = {
                        error: "invalid email"
                    }
                } else {
                    if (ctx.request.body.password.length < 6) {
                        ctx.status = 400
                        ctx.body = {
                            error: "password too short"
                        }
                    } else {
                        let row = await apiModel.checkEmail(pool, ctx.request.body.email)
                        if (row[0].row == 0) {
                            let password = await bcrypt.hash(ctx.request.body.password, 10)
                            let succes = await apiModel.insertUser(pool, [ctx.request.body.email, password])
                            if (succes == true) {
                                let id = await apiModel.getIdByEmail(pool, ctx.request.body.email)
                                ctx.status = 200
                                ctx.body = {
                                    userId: id
                                }
                            }
                        } else {
                            ctx.status = 400
                            ctx.body = {
                                "error": "email already used"
                            }
                        }
                    }
                }
            }
        },
        async signin(ctx) {
            let email = ctx.request.body.email
            let password = ctx.request.body.password
            let result = await apiModel.getAllByEmail(pool, email)
            if (result.length > 0) {
                let check = await bcrypt.compare(password, result[0].password)
                if (check) {
                    ctx.session.id = result[0].id
                    ctx.session.email = result[0].email
                    ctx.status = 200
                    ctx.body = {}
                } else {
                    ctx.status = 400
                    ctx.body = {
                        error: "wrong email or password"
                    }
                }
            } else {
                ctx.status = 400
                ctx.body = {
                    error: "wrong email or password"
                }
            }
        },
        async signOut(ctx) {
            ctx.status = 200
            delete ctx.session.id
            delete ctx.session.email
            ctx.body = {}
        },
        async getPikka(ctx) {
            const allpic = await apiModel.getAll(pool)
            ctx.status = 200
            let list = []
            allpic.forEach((val, index) => {
                list.push(val)
            })
            ctx.body = {list: list}
        },
        async postPikka(ctx) {
            if (!ctx.session.id) {
                ctx.status = 401
                ctx.body = { error: "unauthorized" }
            } else if (typeof ctx.request.files.picture == 'undefined' || ctx.request.files == '') {
                ctx.status = 400
                ctx.body = { error: "picture required" }
            } else if (!ctx.request.body.caption) {
                ctx.status = 400
                ctx.body = { error: "caption required" }
            } else if(typeof ctx.request.files.picture != 'undefined') {
                let filename = uuid4()
                let userId = ctx.session.id
                let pathFile = ctx.request.files.picture.path
                let data = [filename, ctx.request.body.caption, userId]
                let row = await apiModel.apiCreate(pool, data)
                await fs.copy(pathFile, path.join(pictureDir, filename))
                ctx.status = 200
                ctx.body = row
            }
        },
        async getPikById(ctx){
            let pikData = await apiModel.getPikById(pool, ctx.params.id)
            let comment = await apiModel.getCommentById(pool, ctx.params.id)
            let newComment = comment.map((val) => {
                return {id: val.id, text: val.text, createdAt: val.created_at}
            })
            let newObj = {}
            let comments = []
            newObj.id = pikData.id
            newObj.caption = pikData.caption
            newObj.picture = `http://localhost:3000/img/${pikData.picture}`
            newObj.createdAt = pikData.created_at
            newObj.likeCount = pikData.likes
            newObj.comments = newComment
            
            ctx.status = 200
            ctx.body = newObj
        },
        async addComment(ctx){
            if(!ctx.session.id){
                ctx.status = 401
                ctx.body = {error: "unauthorized"}
            }else if(ctx.request.body.text == '' || typeof ctx.request.body.text == 'undefined'){
                ctx.status = 400
                ctx.body = {error: "text required"}
            }else{
                let numPic = await apiModel.countPicById(pool, ctx.params.id)
                if(numPic<1){
                    ctx.status = 400
                    ctx.body = {error: "invalid request"}
                }else{
                    await apiModel.addComment(pool, [ctx.request.body.text, ctx.params.id, ctx.session.id])
                    let viewComment = await apiModel.lastedComment(pool)
                    ctx.status = 200
                    ctx.body = viewComment
                }
            }
        },
        async addLike(ctx){
            if(!ctx.session.id){
                ctx.status = 401
                ctx.body = {error: "unauthorized"}
            }else{
                let numPic = await apiModel.countPicById(pool, ctx.params.id)
                if(numPic<1){
                    ctx.status = 400
                    ctx.body = {error: "invalid request"}
                }else{
                    let num = await apiModel.countLikeByPicId(pool, [ctx.params.id, ctx.session.id])
                    if(num>0){
                        ctx.status = 200
                        ctx.body = {}
                    }else{
                        await apiModel.addLike(pool, [ctx.params.id, ctx.session.id])
                        ctx.status = 200
                        ctx.body = {}
                    }
                }
            }
        },
        async unLike(ctx){
            if(!ctx.session.id){
                ctx.status = 401
                ctx.body = {error: "unauthorized"}
            }else{
                let numPic = await apiModel.countPicById(pool, ctx.params.id)
                if(numPic<1){
                    ctx.status = 400
                    ctx.body = {error: "invalid request"}
                }else{
                    await apiModel.unLike(pool, [ctx.params.id, ctx.session.id])
                    ctx.status = 200
                    ctx.body = {}
                }
            }
        }

    }
}