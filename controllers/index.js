const showviews = require('./../models/index')

module.exports = function (pool) {
    return {
        async showHome(ctx, next) {
            if(ctx.query.caption){
                rows = await showviews.searchCaption(pool, ctx.query.caption)
                await ctx.render('index', {data: rows})
                await next()
            }else{
                rows = await showviews.showAll(pool)
                await next()
                await ctx.render('index', {data: rows, flash: ctx.flash})
            }
        },
        async directPic(ctx, next){
            let id = ctx.params.id
            if(!isNaN(id)){
                row = await showviews.findById(pool, id)
                let comments = await showviews.findComments(pool, id)
                await ctx.render('pikka', {data: row, comments: comments})
            }else{
                ctx.status = 404
            }
        },
        async likeAndComment(ctx, next){
            if(ctx.request.body.process == 'like'){
                //console.log(ctx.request.body)
                let num = await showviews.checkLike(pool, ctx.session.id, ctx.request.body.picId)
                ctx.body = num
            }else if(ctx.request.body.process == 'comment'){
                console.log(ctx.request.body)
                let num = await showviews.addComment(pool, ctx.session.id, ctx.request.body.picId, ctx.request.body.comment)
                ctx.redirect(`/pikka/${ctx.request.body.picId}`)
            }
        },
        async showProfile(ctx){
            let profile = await showviews.getProfileById(pool, ctx.session.id)
            await ctx.render('/profile', {name: `${profile.first_name} ${profile.last_name}`, email: profile.email})
        }
    }
}