const bcrypt = require('bcrypt')
const signUp = require('./../models/signup')

module.exports = function (pool) {
    return {
        async signUpPage(ctx, next) {
            await ctx.render('signup')
            await next()
        },
        async register(ctx, next) {
            let email = ctx.request.body.email
            let password = await bcrypt.hash(ctx.request.body.password, 10)
            await signUp.insertUser(pool, [email, password])
            await ctx.redirect('/signup')
        }
    }
}