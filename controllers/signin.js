const signIn = require('./../models/signin')
const showviews = require('./../models/index')

module.exports = function (pool) {
    return {
        async signInPage(ctx, next) {
            if(ctx.session.id){
                ctx.redirect('/')
            }else{
                await ctx.render('signin',{flash: false})
            }
            await next()
        },
        async signInPost(ctx, next) {
            let userName = ctx.request.body.email
            let passWord = ctx.request.body.password
            let result = await signIn.checkLogin(pool, userName, passWord)
            if(result){
                ctx.session.id = result[0].id
                ctx.session.email = result[0].email
                ctx.session.flash = {success: 'Signed In Success'}
                await next()
                let data = await showviews.showAll(pool)
                await ctx.render('/index', {data: data, flash: ctx.flash})
            }else{
                ctx.session.flash = {error: 'username or password is incorect'}
                await next()
                await ctx.render('/signin', {flash: ctx.flash})
            }
        },
        async facebookLogIn(ctx){
            let email = ctx.request.body.email
            let firstname = ctx.request.body.firstname
            let lastname = ctx.request.body.lastname
            let facebook_user_id = ctx.request.body.facebook_user_id
            let checkEmail = await signIn.checkEmail(pool, email)
            if(checkEmail){
                await signIn.fbRegister(pool, [email, firstname, lastname, facebook_user_id])
                const result = await signIn.fbLogin(pool, email)
                ctx.session.id = result.id
                ctx.session.email = result.email
                let data = await showviews.showAll(pool)
                await ctx.render('/index', {data: data, flash: ''})
            }else{
                const result = await signIn.fbLogin(pool, email)
                ctx.session.id = result.id
                ctx.session.email = result.email
                let data = await showviews.showAll(pool)
                await ctx.render('/index', {data: data, flash: ''})
            }
        }
    }
}