const create = require('./../models/create')

module.exports = function (pool, pictureDir) {
    return {
        async uploadPage(ctx, next) {
            await ctx.render('create')
        },
        async uploadFile(ctx, next) {
            let caption = ctx.request.body.caption
            let pathFile = ctx.request.files.picture.path
            await create.insertPhoto(pool, {caption: caption, pathFile: pathFile,pictureDir: pictureDir, user: ctx.session.id})
            await ctx.redirect('/')
        }
    }
}